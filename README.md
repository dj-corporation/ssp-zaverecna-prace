# Web of dispensing boxes

Website implementation of dispensing boxes in Django framework.  
It is similar to [Zásilkovna](https://www.zasilkovna.cz/en/).

## Authors

- Dominik Lichnovský (LIC0090) @JersyJasper
- Jindřich Tvrdý (TVR0041) @jindrich.tvrdy

## Reports

[![coverage report](https://gitlab.com/dj-corporation/ssp-zaverecna-prace/badges/master/coverage.svg)](https://gitlab.com/dj-corporation/ssp-zaverecna-prace-/commits/master)
[![pipeline status](https://gitlab.com/dj-corporation/ssp-zaverecna-prace/badges/master/pipeline.svg)](https://gitlab.com/dj-corporation/ssp-zaverecna-prace/-/commits/master)

[Reports](https://dj-corporation.gitlab.io/ssp-zaverecna-prace/)

## Documentation

[Documentation](https://dj-corporation.gitlab.io/ssp-mala-prace/)  
[Repository of documentation](https://gitlab.com/dj-corporation/ssp-mala-prace/)

## Development installation

Install python-pip:
(Debian and ubuntu based distros)

```bash
sudo apt install python-pip
```

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install virtualenv.  
Then create venv in python:

```bash
python3 -m pip install --user virtualenv
python3 -m venv VENV_DIR
```

Activate venv

```bash
source VENV_DIR/bin/activate
```

Install requirements:

```bash
pip install -r requirements.txt 
```

Run a development server:

```bash
./manage.py runserver
```

## License

No license


