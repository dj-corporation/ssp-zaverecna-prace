from django.apps import AppConfig


class WebboxesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'webboxes'
