import email
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=50, blank = True)
    zipcode = models.CharField(max_length=10, blank = True)
    city = models.CharField(max_length=30, blank = True)
    def __str__(self):
        return f'{self.username} - {self.first_name} {self.last_name}'
    def cityzip(self):
        return f'{self.city} {self.zipcode}'

class Partition(models.Model):
    name = models.CharField(max_length=6)
    status = models.CharField(max_length=1)
    id_box = models.ForeignKey('Box', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.id}'

class Parcel(models.Model):
    id_receiver = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, related_name='id_receiver', null = True)
    id_sender = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, related_name='id_sender', null = True)
    typeOfParcel = models.ForeignKey('Category', on_delete=models.SET_NULL, related_name='category', null = True)
    id_box = models.ForeignKey('Box', on_delete=models.SET_NULL, null = True)
    password_pickUp = models.CharField(max_length=5, blank = True)
    password_sendIn = models.CharField(max_length=6, blank = True)
    date_store = models.DateTimeField(null = True, blank = True)
    date_toPickedUp = models.DateTimeField(null = True, blank = True)
    date_pickedUp = models.DateTimeField(null = True, blank = True)
    cancel = models.BooleanField(null = True, blank = True)
    id_partition = models.ForeignKey('Partition', on_delete=models.SET_NULL, null = True, blank = True)
    def __str__(self):
        return f'{self.id}'
    
class Category(models.Model):
    name = models.CharField(max_length=40)
    description = models.CharField(max_length=120)
    def __str__(self):
        return f'{self.id} {self.name}'

class Box(models.Model):
    address = models.CharField(max_length=50)
    city = models.CharField(max_length=30)
    status = models.CharField(max_length=1)
    def __str__(self):
        return f'ID: {self.id} - {self.address}, {self.city}'

class Change(models.Model):
    date_store = models.DateTimeField()
    typeOfChange = models.CharField(max_length=20)
    id_parcel = models.ForeignKey('Parcel', on_delete=models.SET_NULL, null = True, blank = True)
