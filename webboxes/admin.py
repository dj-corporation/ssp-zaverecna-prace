from django.contrib import admin
from webboxes.models import Partition, Box, Parcel, Category, User

admin.site.register(User)
admin.site.register(Box)
admin.site.register(Partition)
admin.site.register(Parcel)
admin.site.register(Category)
