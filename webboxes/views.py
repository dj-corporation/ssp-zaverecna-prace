from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from webboxes.models import User, Parcel, Category, Box
from webboxes.forms import LoginForm, RegisterForm, RecoverForm, TrackParcelForm, ParcelSendForm, UserEditForm, MyPasswordChangeForm
from django.contrib.auth import authenticate, update_session_auth_hash, login as djangoLogin, logout as djangoLogout
from django.contrib import messages
import shortuuid
from django.contrib.auth.hashers import check_password, is_password_usable
from django.contrib.auth.decorators import login_required
from django.db.models import Q

def index(request):

    return render(request, 'index.html', {})

def login(request):
    if request.user.is_authenticated:
        return redirect('index')
    if request.method == 'POST':
        loginform = LoginForm(request.POST)
        if loginform.is_valid():
            tempuser = authenticate(username=loginform.cleaned_data['username'], password=loginform.cleaned_data['password'])
            if tempuser is not None:
                djangoLogin(request, tempuser)
                if loginform.cleaned_data['rememberMe'] is False:
                    request.session.set_expiry(600)
                return redirect('index')

            else:
                messages.error(request, 'Username or password is invalid')
                return render(request, 'login.html', {'form': loginform})
    else:
        form = LoginForm()
    
    return render(request, 'login.html', {'form':form})

def register(request):
    if request.user.is_authenticated:
        return redirect('index')
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            djangoLogin(request, user)
            return redirect('index')
    else: 
        form = RegisterForm()

    return render(request, 'register.html', {'form':form})

@login_required(login_url='login')
def logout(request):
    djangoLogout(request)
    return redirect('index')

def recover(request):
    if request.method == 'POST':
        form = RecoverForm(request.POST)
        if form.is_valid():
            try:
                user = User.objects.get(email=form.cleaned_data['email'])
                messages.success(request, 'Recovery email was succesfull sent')
            except User.DoesNotExist:
                form.add_error('email', 'Email does not exist')
    else:    
        form = RecoverForm()

    return render(request, 'recover.html', {'form':form})

@login_required(login_url='login')
def parcel_send(request):
    if request.method == 'POST':
        form = ParcelSendForm(request.POST)
        if form.is_valid():
            receiver_p = form.cleaned_data['id_receiver']
            typeOfParcel_p = form.cleaned_data['typeOfParcel']
            id_box_p = form.cleaned_data['id_box']
            sender_p = request.user
            s = shortuuid.ShortUUID(alphabet="0123456789")
            password_sendIn_p = s.random(length=6)
            parcel = Parcel(id_receiver=receiver_p, id_sender=sender_p, typeOfParcel=typeOfParcel_p, id_box=id_box_p, password_sendIn=password_sendIn_p)
            parcel.save()
            messages.success(request, 'Packet was succesfull created')
            messages.success(request, f'Your Send In Password is {password_sendIn_p}')
    else:
        form = ParcelSendForm()
    return render(request, 'parcel/send.html', {'form':form})


def parcel_track(request):
    if request.method == 'POST':
        form = TrackParcelForm(request.POST)
        if form.is_valid():
            try:
                parcel = Parcel.objects.get(pk=form.cleaned_data['id_parcel'])
                return redirect('track_id', parcel_id=form.cleaned_data['id_parcel'])
            except Parcel.DoesNotExist:
                form.add_error('id_parcel', 'Parcel is not in the system')
    else:
        form = TrackParcelForm()
    return render(request, 'parcel/track.html', {'form':form})


def parcel_track_id(request, parcel_id):
    parcel = get_object_or_404(Parcel, id=parcel_id)

    return render(request, 'parcel/track_id.html', {'parcel': parcel})

def boxes(request):
    boxes_list = Box.objects.all()

    return render(request, 'boxes/boxes.html', {'boxes_list': boxes_list})

@login_required(login_url='login')
def parcel_list(request):
    user = request.user
    parcel_l = Parcel.objects.filter(Q(id_receiver=user) | Q(id_sender=user))
    return render(request, 'parcel/list.html', {'parcel_l': parcel_l})

def box_id(request, box_id):
    box = get_object_or_404(Box, id=box_id)
    if box.status == '1':
        status = 'Functional and not full'
    elif box.status == '2':
        status = 'Functional and full'
    else:
        status = 'Not functional'
    return render(request, 'boxes/box_id.html', {'box':box, 'status':status})

@login_required(login_url='login')
def user_profile(request):
    user = request.user
    if not user.address:
        user.address = 'None'
    if not user.city and not user.zipcode:
        user.city = 'None'
        user.zipcode = 'None'

    return render(request, 'user/profile.html', {'user': user})

@login_required(login_url='login')
def user_profile_edit(request):
    user = request.user
    if request.method == 'POST':
        form = UserEditForm(request.POST, instance = user)
        if form.is_valid():
            form.save()
            return redirect('profile')
    else:
        form = UserEditForm(instance = user)
    return render(request, 'user/edit.html', {'form':form})

@login_required(login_url='login')
def user_change_password(request):
    if request.method == 'POST':
        password = request.user.password
        user = request.user
        form = MyPasswordChangeForm(request.POST, instance = user)
        if form.is_valid():
            form.clean()
            if request.user.is_authenticated:
                if not check_password(form.data['old_password'], password):
                    form.add_error('old_password', 'Your old password is not correct')
                    form.add_error('password', '')
                    form.add_error('reenter_password', '')
                else:
                    if form.data['old_password'] == form.data['password']:
                        form.add_error('old_password', '')
                        form.add_error('password', 'New password is the same as old one')
                        form.add_error('reenter_password', '')
                    if form.data['password'] != form.data['reenter_password']:
                        form.add_error('old_password', '')
                        form.add_error('password', '')
                        form.add_error('reenter_password', 'The reenter password is not same as new')
                    if not is_password_usable(form.data['password']):
                        form.add_error('old_password', '')
                        form.add_error('password', 'Your password is too weak!')
                        form.add_error('reenter_password', '')
                    else:
                        user.set_password(form.data['password'])
                        form.save()
                        update_session_auth_hash(request, user)  # Important!
                        messages.success(request, 'Your password was successfully updated!')
                        return redirect('change_password')
    else:
        form = MyPasswordChangeForm()
    return render(request, 'user/change_password.html', {'form': form})

