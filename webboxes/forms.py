from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import TextInput, EmailInput, PasswordInput, Select
from webboxes.models import User, Parcel

class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'Username'
                }), label="Username", max_length=100)
    password = forms.CharField(widget=forms.TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'Password',
                'type': 'password'
                }), max_length=64, label="Password")
    rememberMe = forms.BooleanField(label="Remember Me", required=False)

class RegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'First name'
                }))
    last_name = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'Last name'
                }))
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.', widget=forms.EmailInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'Email'
                }))
    phone = forms.CharField(max_length=20, help_text='Required.', widget=forms.TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'Phone'
                }))
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'phone', 'password1', 'password2']
    
    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)

        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['username'].widget.attrs['style'] = 'max-width: 300px;'
        self.fields['username'].widget.attrs['placeholder'] = 'Username'
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['style'] = 'max-width: 300px;'
        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['style'] = 'max-width: 300px;'

class RecoverForm(forms.Form):
    email = forms.EmailField(max_length=254, help_text='Inform a valid email address.', widget=forms.EmailInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'Email'
                }))

class TrackParcelForm(forms.Form):
    id_parcel = forms.CharField(widget=forms.TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': '123 456 789'
                }), label="Parcel ID", max_length=12)
    class Meta:
        model = Parcel
        fields = ['id_parcel']

class ParcelSendForm(forms.ModelForm):
    class Meta:
        model = Parcel
        fields = ['id_receiver', 'typeOfParcel', 'id_box']
        labels = { 'id_receiver':'Receiver', 'typeOfParcel':'Type of parcel', 'id_box':'Box ID'}
        widgets = {
            'id_receiver': Select(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'Receiver'
                }),
            'typeOfParcel': Select(attrs={
                'class': "form-control", 
                'style': 'max-width: 300px;',
                'placeholder': 'Type of Parcel'
                }),
            'id_box': Select(attrs={
                'class': "form-control", 
                'style': 'max-width: 400px;',
                'placeholder': 'Box ID'
                })
        }
class UserEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'phone', 'address', 'city', 'zipcode' ]
        labels = { 'username':'Username', 'first_name':'First name', 'last_name':'Last name', 'email':'Email', 'phone':'Phone', 'address': 'Address', 'zipcode':'Zip code', 'city':'City'}
        widgets = {
            'username': TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'Username'
                }),
            'first_name': TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'First name'
                }),
            'last_name': TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'Last name'
                }),
            'email': EmailInput(attrs={
                'class': "form-control",
                'style': 'max-width: 400px;',
                'placeholder': 'Email'
                }),
            'phone': TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'Phone'
                }),
            'address': TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 400px;',
                'placeholder': 'Address'
                }),
            'city': TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'City'
                }),
            'zipcode': TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 200px;',
                'placeholder': 'Zip code'
                })
        }
        help_texts = {
            'username': 'Required.',
            'first_name': 'Optional.',
            'last_name': 'Optional.',
            'email': 'Required.',
            'phone': 'Required.',
            'address': 'Optional',
            'city': 'Optional.',
            'zipcode': 'Optional.'
        }

class MyPasswordChangeForm(forms.ModelForm):
    old_password=forms.CharField(widget=forms.PasswordInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                }), label="Old password", max_length=64)
    password=forms.CharField(widget=forms.PasswordInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                }), label="New password", max_length=64)
    reenter_password=forms.CharField(widget=forms.PasswordInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                }), label="Reenter new password", max_length=64)
    def clean(self):
        old_password=self.cleaned_data.get('old_password')
        password=self.cleaned_data.get('password')
        reenter_password=self.cleaned_data.get('reenter_password')
        return self.cleaned_data
    class Meta:
        model = User
        fields = ['old_password', 'password', 'reenter_password']
    
