"""dispensingboxes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from webboxes import views 

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('register/', views.register, name='register'),
    path('recover/', views.recover, name='recover'),
    path('parcel/list/', views.parcel_list, name='parcel_list'),
    path('parcel/send/', views.parcel_send, name='send'),
    path('parcel/track/', views.parcel_track, name='track'),
    path('parcel/track/<int:parcel_id>/', views.parcel_track_id, name='track_id'),
    path('boxes/', views.boxes, name='boxes'),
    path('boxes/<int:box_id>/', views.box_id, name='box_id'),
    path('user/profile/', views.user_profile, name='profile'),
    path('user/edit/', views.user_profile_edit, name='profile_edit'),
    path('user/change-password/', views.user_change_password, name='change_password'),

]
